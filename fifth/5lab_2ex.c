/*калейдоскоп*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#define N 39  //размер массива
#define M 120 //плотность заполнения символами
#define S 3   //чаровский символ символ '*' - 42

void printStar (char arr[][N]);  //сучайное заполнение массива звездочками
void printArray (char arr[][N]); //вывод массива на экран
void copyQurter (char arr[][N]); //симметричное копирование квадрантов
void clearArray (char arr[][N]); //заполнение массива пробелами

int main()
{
	char arr[N][N]={32};
	int i=0;

	printf("Enter the number of repetitions:  ");
	scanf("%d", &i);
	if(i<1)
	{
		printf("Error! Value was entered incorrectly.");
		exit(1);
	}
	srand((unsigned)time(NULL));
	for(i;i>0;i--)
	{
		system("cls");
		printStar(arr);
		copyQurter(arr);
		printArray(arr);
		Sleep(1000);
		clearArray(arr);
	}
	return 0;
}

void printStar (char arr[][N])
{ 
	int i, j, star;
	for(star=0;star<M;star++)
	{
		i=rand()%(N/2);
		j=rand()%(N/2);
		arr[i][j]=S;
	}
}

void printArray (char arr[][N])
{
	int i, j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<N;j++) 
			printf("%c ",arr[i][j]);
		putchar('\n');
	}
}

void copyQurter (char arr[][N])
{
	int i, j;
	//заполняем верхний правый квадрант
	for(i=0;i<N/2;i++)
	{
		for(j=0;j<N/2;j++)
			arr[i][N-1-j]=arr[i][j];
	}
	//заполняем нижние квадранты
	for(i=0;i<N/2;i++)
	{
		for(j=0;j<N;j++)
			arr[N-1-i][j]=arr[i][j];
	}
}

void clearArray (char arr[][N])
{
	int i, j;
	for(i=0;i<N;i++)
	{
		for(j=0;j<N;j++) 
			arr[i][j]=32;
	}
}


