/* запросить рост, вес и пол. По формуле посчитать коэффициент веса и дать совет: похудеть, потолстеть или вес в норме
*/
# include <stdio.h>
# include <math.h>

int main()
{
	const double w1 = 19., w2 = 24., m1 = 19., m2 = 25.;
	char sex;
	double height, weight, ind;
	printf("Enter sex w/m: \n");
	scanf("%c", &sex);
	printf("Enter height \n");
	scanf("%lf", &height);
	printf("Enter weight \n");
	scanf("%lf", &weight);
	printf("sex = %c; height = %6.2lf; weight = %6.2lf.\n", sex, height, weight);
	ind = weight / (height * height/10000.);
	printf("ind = %6.2lf\n", ind);
	if(sex == 'w')
	{
		if(ind >= w1 && ind <= w2)
			printf("standart\n");
		else if(ind < w1)
			printf("drow fat\n");
		else
			printf("grow thin\n");
	}
	else if(sex == 'm')
	{
		if(ind >= m1 && ind <= m2)
			printf("standart\n");
		else if(ind < m1)
			printf("drow fat\n");
		else
			printf("grow thin\n");
	}
	else
		printf("Sex not found!\n");
	return 0;
}
