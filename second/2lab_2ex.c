/*������ ���������� ������� 1 �� 100, ������������ ��� ���������*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main ()
{
	int number, popytka=1, usernumber;

	srand((unsigned)time(NULL));
	number = 1 + rand()%100;
	printf("Ugaday chislo(1-100)!\n");

	while (1)  {
		printf("%d popytka: ", popytka++);
		scanf("%d", &usernumber);
		if(usernumber<number) {
			printf("ne ugadal, povtori popytku, tvoe chislo menshe zagadannogo!\n");
			continue;
		}
		else if(usernumber>number) {
			printf("ne ugadal, povtori popytku, tvoe chislo bolshe zagadannogo!\n");
	    	continue;
		}
		else if(usernumber==number) {
			printf("Ugadal!\n");
			break;
		}
	}

	return 0;
}