/*  ����� ������, ���������� ����� �����������
 *  ���� � ������, ��������� �������������.*/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>
#define N 80

int getWords(char buf[], char *ptr[]);
void printWord(char *point);
void shuffleWord(char *ptr[], int words);
		
int main()
{
	char buf[N]={0}, *ptr[N/4]={0};
	int i=0, word=0;

	srand((unsigned)time(NULL));
	printf("Enter your string.\n");
	gets(buf);

	word=getWords(buf, ptr);
	shuffleWord(ptr,word);
	for(i=0;i<word;i++)
		printWord(ptr[i]);
	putchar('\n');

	return 0;
}
//���������� ������� ����������
int getWords(char buf[], char *ptr[])
{
	int i=0, flag=0, word=0;
	while(buf[i])
	{
		if(isalpha(buf[i]) && flag==0)
		{
			ptr[word]=&buf[i];
			flag=1;
			word++;
		}
		else if(!(isalpha(buf[i])))
			flag=0;
		i++;
	}
	return word;
}
//������ ������ �����
void printWord(char *point)
{
	int i=0;
	while(isalpha(*(point+i)))
	{
		putchar(*(point+i));
		i++;
	}
	putchar(32);
}
//������������� ���� (���������� � �������)
void shuffleWord(char *ptr[], int words)
{
	int i,j;
	char *temp;

	for(i=0;i<words;i++)
	{
		j=rand()%(words);
		temp=ptr[i];
		ptr[i]=ptr[j];
		ptr[j]=temp;
	}
}
