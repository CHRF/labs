/* �������� ���������, ������� ����������� ���������� 
*  ������������� � �����. ����� ��������� ������ ����� � �������. 
*  ���������� ������ ������� � ������ ������� ������������.
*/
#include <stdio.h>
#define H 100
#define W 32
int main()
{
	char arr[H][W]={0};
	int i, minage=0, maxage=0, age=0, count=0;
	char *young, *old;

	printf("Enter number of ralatives.\n");
	scanf("%d", &count);
	if (count < 1)  {
		printf("Error. You don't have ralatives?\n");
		exit(1);
	}

	for(i=0; i<count; i++) {
		printf("Enter the name your %d ralative and his(her) age.\n", i+1);
		scanf("%s", arr[i]);
		scanf("%d", &age);
		if(age<minage || minage==0)  {
			minage=age;
			young=arr[i];
		}
		if(age>maxage || maxage==0)  {
			maxage=age;
			old=arr[i];
		}
	}
		printf("The youngest relative in your family is %s.\n", young);
		printf("The oldest relative in your family is %s.\n", old);

	return 0;
}