/*������� ������� ���������� N-�� �������� ���� ��������
  ���������� ��������, �� �������� ������������������
  ������� ������ �����������  */
#include<stdio.h>
#include<time.h>
#define DIA 40

int fib(int i);

int main()
{
	int i, n=0;
	int fibsum;
	clock_t beg,end;
	double time;
	FILE *out;

	if(!(out=fopen("fib.txt", "w")))
	{
		printf("Error! I cann't opened file fib.txt.");
		exit(1);
	}
	printf("Enter your number (for 1 to 40).\n");
	scanf("%d", &n);

	if(n<=0 || n>DIA)
	{
		printf("Error! You wrote uncorrect n.\n");
		exit(2);
	}

	for(i=1;i<=n;i++)
	{
		beg=clock();
		fibsum=fib(i);
		end=clock();
		time=(double)(end-beg)/CLOCKS_PER_SEC;
		printf("%4d\t%10d\t%6.3f\n",i, fibsum, time);
		fprintf(out, "%4d\t%10d\t%6.3f\n",i, fibsum, time);
	}
	fclose(out);
	return 0;
}

int fib(int i)
{ 
	if(i==0 || i==1)
		return i;
	else
		return fib(i-1)+fib(i-2);
}


