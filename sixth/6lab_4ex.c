/*суммирование элементов числового массива 
рекурсивным способом и традиционным (итерация)*/
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<ctype.h>

int sqr(int m);
int sumiter(int *buf, int n);
int sumrec(int *buf, int n);

int main(int argc, char *argv[])
{
	int i, m=0, n=0, *buf, si, sr;
	clock_t beg, end;
	double titer, trec;

	if(argc!=2)
	{
		puts("Error. You entered uncorrect arguments.");
		exit(1);
	}
	srand((unsigned)time(NULL));
	//преобразуем аргумент, вычисляем размер массива и выделяем под него память
	m=atoi(argv[1]);
	n=sqr(m);
	buf=(int*)(malloc(sizeof(int)*n));
	for(i=0;i<n;i++) //заполняем массив случайными числами
		buf[i]=rand()%1000;
	//вычисляем сумму итерацией и фиксируем время
	beg=clock();
	si=sumiter(buf, n);
	end=clock();
	titer=(double)(end-beg)/CLOCKS_PER_SEC;
	//вычисляем сумму рекурсионной функцией и фиксируем время
	beg=clock();
	sr=sumrec(buf, n);
	end=clock();
	trec=(double)(end-beg)/CLOCKS_PER_SEC;
	printf("Summa iteration is %d, time= %.3f\nSumma recurs is %d, time= %.3f\n", si, titer, sr, trec);

	if(trec>titer)
		puts("Iteration is faster recursion!");
	else
		puts("Recursion is faster iteration!");
	
	free(buf);
	return 0;
}

int sqr(int m)
{
	if(m==0)
		return 1;
	else
		return 2*sqr(m-1);
}

int sumiter(int *buf, int n)
{
	int i, summ=0;
	for(i=0;i<n;i++)
		summ+=buf[i];
	return summ;
}
int sumrec(int *buf, int n)
{
	if(n==1)
		return buf[0];
	else
		return (sumrec(buf, n/2)+sumrec(buf+n/2, n-n/2)); 
}