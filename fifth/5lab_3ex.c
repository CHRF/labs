/*��������� ������������ �������� �����. ������������
�������� ��� ������� ����� ����� ������� � ����������. */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 1024

int getWord(char *arr, char *parr[N]);
void printWords(char *p);

int main()
{
	char arr[N]={0}, *parr[N/4]={0};
	int ch=0, n=0, nw=0, i=0;
	FILE *in;

	in = fopen("in.txt", "r");
	if(in == NULL)
	{
		printf("Error. I can't open file for read.\n");
		exit(1);
	}
	srand((unsigned)time(NULL));
	while((ch = fgetc(in)) != EOF)
	{
		if(ch == '\n')
		{
			arr[n]='\0';
			n=0;
			printf("%s\n", arr);
			nw=getWord(arr, parr);
			for(i=0; i<nw; i++)
				printWords(parr[i]);
			putchar('\n');
		}
		else
			arr[n++]=(char)ch;
	}
	fclose(in);

	return 0;
}

int getWord(char *arr, char *parr[N])
{
	int newword=0, fl=0;
	char *p;
	

	p=arr;
	while(*p)
	{
		if(*p != ' ' && fl == 0)
		{
			fl=1;
			parr[newword++]=p;
		}
		else if(*p == ' ' && fl == 1)
		{
			fl=0;
			*p='\0';
		}
		p++;
	}
	return newword;
}
void printWords(char *p)
{
	int i, temp, len = strlen(p);
	
	if(len < 4) 
		printf("%s ", p);
	else
	{
		putchar(*p);
		p++;
		i=len=len-2;
		while(i)
		{
			temp=rand()%len;
			if(*(p + temp) != '\0')
			{
				putchar(*(p + temp));
				*(p + temp)='\0';
				i--;
			}
		}
		putchar(*(p+len));
		putchar(' ');
	}
}