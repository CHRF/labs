#ifndef _HFILE_H_
#define _HFILE_H_
#define ASC 256
#define MBU 7

typedef unsigned char UnsC;
typedef struct byte 
{
	int ch;
	long friq;
	char str[MBU];
	struct byte *left;
	struct byte *right;
} Byte;
struct st_char 
{
	UnsC b0 : 1;
	UnsC b1 : 1;
	UnsC b2 : 1;
	UnsC b3 : 1;
	UnsC b4 : 1;
	UnsC b5 : 1;
	UnsC b6 : 1;
	UnsC b7 : 1;
};
typedef union bit_char 
{
	UnsC dig;
	struct st_char bits;
} Bit_char;

void unzip(FILE *fp, char *name);
void arkh(FILE *fp, char *name);
Byte * builTree(Byte **ptrarr, int count);
void free_tree(Byte *root);
void complitArr(Byte arr[], FILE *fp);
int mycomp(const void *p1, const void *p2);
void sortPtrArr(Byte **ptrarr, int count);
void generate(Byte *root, char *s);
UnsC FileToStr(FILE *fp, FILE *ftemp, Byte *root, Byte *arr);
char *newName(char *name, char *newname, char *old_extension);
FILE *createHead(FILE *farkh, const char *format, UnsC count, Byte arr[], UnsC tail, char *old_extension);
void strToFile(FILE *farkh, FILE *ftemp);

#endif