/*�������������� ������ ����� � ������ 
��� ������������ ������������ �������*/
#include<stdio.h>
#define N 16

int fun(int number, int *ptr);

int main()
{
	int number, chsimb, count=0, div, i=0;
	char buf[N]={0};

	printf("Enter your number:  \n");
	scanf("%d", &number);
	do
	{
		div=1;
		chsimb=fun(number, &div);
		number=number-chsimb*div;
		buf[count]=(char)('0' + chsimb);
		count++;
	}
	while(number);
	while(buf[i])
	{
		printf("%c\n", buf[i]);
		i++;
	}
	return 0;
}

int fun(int number, int *ptr)
{
	if(number/(*ptr)<10)
		return(number/(*ptr));
	else
	{
		(*ptr)=(*ptr)*10;
		return(fun(number, ptr));
	}
}