/*определение палиндрома*/
#include <stdio.h>
#include <string.h>
#define N 80
int main()
{
	char *st, *end, buf[N]={0};

	printf("Enter your string.\n");
	gets(buf);
	st = buf;
	end = buf + strlen(buf) - 1;
	while(end > st)  {
		if(*end != *st)  {
			printf("The string is not palindrome.\n");
			exit(1);
		}
		st++; end--;
	}
	printf("The string is palindrome.\n");
	return 0;
}