/*����� ���������� ������������� �������� ��� ��������� ������������� ������. 
���������� ��������� �� ��������, �.�. ����� ���������� - ������.*/
#include <stdio.h>
#define N 80
#define S 256

typedef struct LettersStat LetStat;

struct LettersStat {
	char lette;
	int count;
};

void sort(LetStat arr[])  {

	int j=0, m, n;
	LetStat temp;

	while(arr[j].count)
		j++;

	for(m=0;m<j;m++) {
		for(n=j-1;n>m;n--){
			if((arr[n-1].count)<(arr[n].count)) {
				temp=arr[n-1];
				arr[n-1]=arr[n];
				arr[n]=temp;
			}
		}
	}
}
void printArr(LetStat arr[]) {

	int i=0;
	while(arr[i].count) {
		printf("'%c' -%d\n", arr[i].lette, arr[i].count);
		i++;
	}
}

int main()
{
	LetStat let, arr[S]={0};
	char lette=0, buf[N]={0};
	int i=0, j=0;

	printf("Enter your string: ");
	gets(buf);

	while(buf[i]) {
		lette=buf[i];
		for(j=0;j<S;j++) {
			if(arr[j].count==0)
				break;
			if((arr[j].lette)==lette) {
				arr[j].count++;
				lette=0;
			}
		}
		if(lette!=0) {
			let.lette=lette;
			let.count=1;
			arr[j]=let;
		}
		lette=0; i++;
	}
	sort(arr);
	printArr(arr);

	return 0;
}