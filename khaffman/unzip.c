/*���������� ����� ���������� kha*/
#include <stdio.h>
#include <string.h>
#include "head.h"
#include <stdlib.h>

const char format[3];

void unzip(FILE *fp, char *name)
{
	char our_format[4];
	UnsC count, temp, tail;
	int i, ch;
	Byte *arr, **ptrarr, *root, *pnode;
	char s[8], old_extension[MBU], *old_name;
	FILE *ftemp, *fsource;
	Bit_char sym;

	fread(our_format, 1, 3, fp);
	our_format[3]='\0';
	if(strcmp(our_format, format)!=0)
	{
		puts("I can not unzip your file.\n");
		exit(4);
	}
	fread(&count, 1, 1, fp);
	arr=(Byte *)malloc((count+1) * sizeof(Byte));
	ptrarr=(Byte **)malloc((count+1) * sizeof(Byte *));
	for(i=0; i<=count; i++)
	{
		fread(&temp, 1, 1, fp);
		arr[i].ch=(int)temp;
		arr[i].right=arr[i].left=NULL;
		fread(&arr[i].friq, sizeof(long), 1, fp);
		ptrarr[i]=arr + i;
	}
	root=builTree(ptrarr, count);
	free(ptrarr);
	fread(&tail, 1, 1, fp);
	fread(&old_extension, 1, MBU, fp);
	ftemp=fopen("temp2.txt", "wb");
	while((ch=getc(fp))!=EOF)
	{
		sym.dig = (UnsC)ch;
		s[0]=sym.bits.b0+'0';
		s[1]=sym.bits.b1+'0';
		s[2]=sym.bits.b2+'0';
		s[3]=sym.bits.b3+'0';
		s[4]=sym.bits.b4+'0';
		s[5]=sym.bits.b5+'0';
		s[6]=sym.bits.b6+'0';
		s[7]=sym.bits.b7+'0';
		fwrite(s, 1, 8, ftemp);
	}
	fclose(fp);
	fseek(ftemp, -(long)tail, SEEK_END);
	putc('#', ftemp);
	fclose(ftemp);
	old_name = (char *)malloc(ASC);
	strcpy(old_name, name);
	old_name[strlen(old_name) - 4]='\0';
	strcat(old_name, old_extension);
	fsource = fopen(old_name, "wb");
	ftemp=fopen("temp2.txt", "rb");
	pnode=root;
	while((ch=getc(ftemp))!='#')
	{
		if(ch=='0')
			pnode=pnode->left;
		else
			pnode=pnode->right;
		if(pnode->left==NULL && pnode->right==NULL)
		{
			putc(pnode->ch, fsource);
			pnode=root;
		}
	}
	fclose(fsource);
	fclose(ftemp);
	remove("temp2.txt");
	free(arr);
	puts("File restored.");
}