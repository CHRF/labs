/*����� ���� ������ � �������� �������*/
#include <stdio.h>
#include <string.h>
#define N 256
int main()
{
	int i, word=0, flag=0;
	char str[N]={0}, *ptr[N/4], *temp;

	printf("Enter your string.\n");
	gets(str);
	if(strlen(str)==0)  {
		printf("Error! Your string is empty!\n");
		exit(1);
	}
	temp = str;
	while(*temp)  {
		if(*temp!=' ' && flag==0)  {
			ptr[word]=temp;
			word++;
			flag=1;
		}
		else if(*temp==' ' && flag==1)  {
				*temp='\0';
				flag=0;
		}
		temp++;
	}
	printf("New string:\n");
	for(i=word-1; i>=0; i--)
		printf("%s ", ptr[i]);
	printf("\n");

	return 0;
}