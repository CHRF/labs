/*���������� ������ ����� � ������������� ���� � ������ ������.*/
#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<time.h>
#define N 256

int getWords(char buf[], char *ptrWord[]);
void printWord(char *point, FILE *out);
void shuffleWord(char *ptrWord[], int words);

int main ()
{
	FILE *in=0, *out=0;
	char buf[N]={0}, *ptrWord[N/4]={0};
	int ch=0, i=0, word=0, j;

	in=fopen("in.txt", "r");
	if(in==0)
	{
		printf("Error. I can't open file (in.txt) for read.\n");
		exit(1);
	}
	out=fopen("out.txt", "w");
	if(out==0)
	{
		printf("Error. I can't open file (out.txt) for writen.\n");
		exit(2);
	}
	srand((unsigned)time(NULL));
	while((ch=fgetc(in)) != EOF)
	{
		if(ch == '\n')
		{
			buf[i]='\0';
			i=0;
			word=getWords(buf, ptrWord);
			shuffleWord(ptrWord,word);
			for(j=0;j<word;j++)
				printWord(ptrWord[j], out);
			putc('\n',out);

		}
		else
			buf[i++]=(char)ch;
	}
	fclose(in);
	fclose(out);
	printf("It's all. Rezult is in file out.txt.\n");

	return 0;
}
//���������� ������� ����������
int getWords(char buf[], char *ptrWord[])
{
	int i=0, flag=0, word=0;
	while(buf[i])
	{
		if(isalpha(buf[i]) && flag==0)
		{
			ptrWord[word]=&buf[i];
			flag=1;
			word++;
		}
		else if(!(isalpha(buf[i])))
			flag=0;
		i++;
	}
	return word;
}
//������ ������ �����
void printWord(char *point, FILE *out)
{
	int i=0;
	while(isalpha(*(point+i)))
	{
		putc((*(point+i)), out);
		i++;
	}
	putc(' ', out);
}
//������������� ���� (���������� � �������)
void shuffleWord(char *ptrWord[], int words)
{
	int i,j;
	char *temp;

	for(i=0;i<words;i++)
	{
		j=rand()%(words);
		temp=ptrWord[i];
		ptrWord[i]=ptrWord[j];
		ptrWord[j]=temp;
	}
}