/*����� ����� �� ����������� ���������� ��������.
����� ������� �� �����, ��������� ������������ � ����*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define H 80
#define W 100

int compare(const void *a, const void *b)  {
	return strlen(*(char **)a) - strlen(*(char **)b);
}
int main(int argc, char *argv[])
{
	char arr[W][H]={0}, *ptr[W], *p, ch;
	int i=0, j=0, nstr;
	FILE *in, *out;

	if (argc<2) {
		printf("Error. No arguments.\n");
		exit(1);
	}
	if((in = fopen(argv[1], "r"))==0)  {
		printf("Error. I coudn't open the file \"%s\".\n", argv[1]);
		exit(2);
	}
	ptr[i] = arr[i];
	while((ch = getc(in)) != EOF)  {
		arr[i][j] = ch;
		j++;
		if (ch == '\n')  {	
			arr[i][j] = '\0';
			i++;
			ptr[i] = arr[i];
			j = 0;
		}
	}
	arr[i][j] = '\0';
	if (fclose(in) != 0)  {
		printf("Error in closing file.\n");
		exit(3);
	}
	nstr = i;
	qsort(ptr, nstr, sizeof(char *), compare);
	if ((out = fopen("result.txt", "w"))==0)  {
		printf("Error! I coudn't open result.txt file.\n");
		exit(2);
	}
	for(i=0; i<=nstr; i++)  {	
		p=ptr[i];
		while(*p)
			putc(*p++, out);
		putc('\n', out);
	}
	if (fclose(out) != 0)
		printf("Error in closing file.\n");
	printf("It is all. Look on the file \"rezult.txt\"");

	return 0;
}