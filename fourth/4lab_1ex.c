/*����� �����, ��������� �������������, �� ����������� ���������� �������� � ������*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define H 30
#define W 80

int compare(const char *a, const char *b)
{
	return strlen(*(char **)a) - strlen(*(char **)b);
}

int main()
{
	char arr[W][H]={0};
	char *parr[W];
	int i= -1, str;

	printf("Enter your text.\n");
	i = -1;
	do  {
		i++;
		gets(arr[i]);
		parr[i] = arr[i];
	}
	while (strlen(arr[i]) > 0);

	str = i;
	if (str <= 0)  {
		printf("Error! You didn't enter a string!\n");
		exit(1);
	}
	qsort(parr, str, sizeof(char *), compare);
	printf("Result:\n");
	for(i = 0; i < str; i++)
		printf(" %s\n", parr[i]);

	return 0;
}

