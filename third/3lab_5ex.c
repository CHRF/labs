/*����������� ����� ��������� ������������� �������, 
������������� ����� ����� ������������� ������ � ��������� 
�������������. � ����� �� �������� "�����������" ��������.*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 10

void fillArray(int arr[])
{
	int sign, numb, i;

	for(i=0;i<N;i++)  {
		sign =rand()%2;
		numb=rand()%100;
		if(sign==0)
			arr[i]= numb * (-1);
		else if(sign==1)
			arr[i]=numb;
	}
}

void printArray(int arr[])
{
	int i;
	printf("Array: ");
	for(i=0;i<N;i++)
		printf("%d ", arr[i]);
	putchar('\n');
}

int main()
{
	int arr[N]={0}, pmax=N-1, pmin=0, i, summ=0;

	srand((unsigned)time(NULL));
	fillArray(arr);
	printArray(arr);

	while(arr[pmin]>0)
		pmin++;
	while(arr[pmax]<0)
		pmax--;

	if(pmin>pmax)
		printf("All positive numbers are locat to the left of the negative!\n");
	else {
		for(i=pmin+1;i<pmax;i++) 
			summ+=arr[i];
		printf("Summ: %d\n",summ);
	}
	return 0;
}