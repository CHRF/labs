#include <stdio.h>
#include "head.h"
#include <string.h>
#include <stdlib.h>

void free_tree(Byte *root)
{
	if(root->left != NULL)
		free_tree(root->left);
	if(root->right != NULL)
		free_tree(root->right);
	free(root);
}
void strToFile(FILE *farkh, FILE *ftemp)
{
	Bit_char sym;
	char s[8];

	while(fread(s, 1, 8, ftemp))
	{
		sym.bits.b0=s[0];
		sym.bits.b1=s[1];
		sym.bits.b2=s[2];
		sym.bits.b3=s[3];
		sym.bits.b4=s[4];
		sym.bits.b5=s[5];
		sym.bits.b6=s[6];
		sym.bits.b7=s[7];
		fwrite(&sym.dig, 1, 1, farkh);
	}
}
FILE *createHead(FILE *farkh, const char *format, UnsC count, Byte arr[], UnsC tail, char *old_extension)
{
	int i;
	UnsC temp;
	long temp2;

	fwrite(format, 1, 3, farkh);
	fwrite(&count, 1, 1, farkh);
	for(i=0; i<=count; i++)
	{
		temp=(UnsC)arr[i].ch;
		fwrite(&temp, 1, 1, farkh);
		temp2=arr[i].friq;
		fwrite(&temp2, sizeof(long), 1, farkh);
	}
	fwrite(&tail, 1, 1, farkh);
	fwrite(old_extension, 1, MBU, farkh);

	return farkh;
}
char *newName(char *name, char *newname, char *old_extension)
{
	int i, len;

	strcpy(newname, name);
	len=strlen(newname) - 1;
	old_extension[0]='\0';
	for(i=len; i>=0; i--)
		if(newname[i]=='.')
		{
			strcpy(old_extension, newname + i);
			strcpy(newname+i, ".kha\0");
			break;
		}
	
	return newname;
}
UnsC FileToStr(FILE *fp, FILE *ftemp, Byte *root, Byte *arr)
{
	int ch, i, m=0;
	char *p;

	rewind(fp);
	while((ch=getc(fp))!=EOF)
	{
		for(i=0; i<ASC; i++)
			if(arr[i].ch==ch)
				break;
		p=arr[i].str;
		while(*p)
		{
			m++;
			putc(*p, ftemp);
			p++;
		}
	}
	for(i=0; i<(8-(m%8)); i++)
		putc('0', ftemp);

	return (UnsC)(8-(m%8));
}
void generate(Byte *root, char *s)
{
	char *news;

	if(root->right==NULL && root->left==NULL)
	{
		strcpy(root->str, s);
		return;
	}
	if(root->right!=NULL)
	{
		news=(char *)malloc(ASC);
		news[0]='\0';
		strcpy(news, s);
		strcat(news, "1\0");
		generate(root->right, news);
		free(news);
	}
	if(root->left!=NULL)
	{
		news=(char *)malloc(ASC);
		news[0]='\0';
		strcpy(news, s);
		strcat(news, "0\0");
		generate(root->left, news);
		free(news);
	}
}
void complitArr(Byte arr[], FILE *fp)
{
	int ch;
	int m=0;

	while((ch=getc(fp))!=EOF)
	{
		arr[ch].friq++;
		m++;
	}
}
int mycomp(const void *p1, const void *p2)
{
	const Byte *a=(const Byte *)p1;
	const Byte *b=(const Byte *)p2;

	if(a->friq < b->friq)
		return 1;
	if(a->friq==b->friq)
		return 0;
	return -1;
}
Byte *builTree(Byte **ptrarr, int count)
{
	Byte *temp;

	temp=(Byte *)malloc(sizeof(Byte));
	temp->left=ptrarr[count - 1];
	temp->right=ptrarr[count];
	temp->friq=ptrarr[count-1]->friq+ptrarr[count]->friq;
	(temp->str)[0]='\0';
	count--;
	ptrarr[count]=temp;
	sortPtrArr(ptrarr, count);
	if(count==0)
		return temp;

	return builTree(ptrarr, count);
}
void sortPtrArr(Byte **ptrarr, int count)
{
	Byte *temp;

	if(count==0)
		return;
	if(ptrarr[count]->friq>ptrarr[count-1]->friq)
	{
		temp=ptrarr[count];
		ptrarr[count]=ptrarr[count-1];
		ptrarr[count-1]=temp;
		count--;
		sortPtrArr(ptrarr, count);
	}
}