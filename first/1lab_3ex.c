/*������� ������ � ������� � ������� */

#include <stdio.h>

int main() {
	   
	const float pi = 3.14159;
	const float max_rad = 2 * pi;
	float val;
	char ch;

	printf("Enter number and letter:\n"
		"d (or D) - degree or r (or R) - radian\n\n");
	scanf("%f%c", &val, &ch);
		
	switch (ch) {

		case 'D': case 'd':
			if (val >= 0 && val <= 360)
				printf("%5.2f rad\n\n", val * pi / 180);
			else 
				printf("Enter number from 0 to 360\n");
		break;
		case 'R': case 'r':
			if (val >= 0 && val <= max_rad)
				printf("-%3.0f%c\n\n", (val * 180 / pi), 248);
			else 
				printf("Enter number from 0 to %4.2f\n", max_rad);
		break;
		default:
			printf("Incorrect letter grade or radian entered.\n");
			printf("Enter a new value.\n\n");
		break;
	}
				
	return 0;
}